import json
from base64 import b64decode
from sys import argv

print(argv)

outdir = argv[3]
outStr = f"{outdir}{argv[2]}_{{}}.pdf"
all_parts_name = f"{argv[2]}_full_score.pdf"


def part_name_processing(s):
    """
    Rend les noms de partoche plus ascii friendly
    """
    s = s.replace("♭", "b")
    s = s.replace(" ", "_")
    return s


with open(argv[1], encoding="utf8") as f:
    data = json.load(f)

for i in range(len(data["parts"])):
    fname = outStr.format(part_name_processing(data["parts"][i]))
    print(f"Writing part {data['parts'][i]} to `{outdir}{fname}`")
    with open(fname, "wb") as f:
        f.write(b64decode(data["partsBin"][i]))
with open(all_parts_name, "wb") as f:
    f.write(b64decode(data["scoreBin"]))
