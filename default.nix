{ pkgs ? (import ./nix { }) }:
let
  exportJsonPartsToPdf = pkgs.writeShellScript "export-json-parts-to-pdf"
    "${pkgs.python3}/bin/python ${./jsonPartsToPdf.py} $@";
  mkParts = { source, mspath ? "${pkgs.musescore}/bin/musescore", debug ? false }:
    pkgs.runCommandNoCC "musescore-${source.name}" { inherit (source) name; } ''
      mkdir -p .local/share
      mkdir pdf_outdir
      QT_QPA_PLATFORM=offscreen XDG_DATA_HOME=$(pwd)/.local/share ${mspath} --score-parts-pdf ${source.file} > parts-pdf.json
      ${exportJsonPartsToPdf} parts-pdf.json ${source.name} pdf_outdir/
      ${if debug then "ls -lah pdf_outdir/" else ""}
      mkdir -p $out/parts_pdf
      mv pdf_outdir/*.pdf $out/parts_pdf
      mv ${source.name}_full_score.pdf $out/

      ${if builtins.match ".+\\.mscz" (builtins.toString source.file) == null then ''
        # QT_QPA_PLATFORM=offscreen XDG_DATA_HOME=$(pwd)/.local/share ${mspath} -o $out/${source.name}.mscz ${source.file}
      '' else ''
        cp ${source.file} $out/${source.name}.mscz
      ''}
    '';
  test = mkParts {
    source = {
      file = ./test.mscz;
      name = "test";
    };
  };
in { inherit test mkParts; }
