# Musescore nix export

Scripts d'export des partitions musescore vers pdf.

## Comportement de musescore concernant les parties

Si le fichier contient des parties alors le script exporte les parties définies dans le fichier musescore

Si le fichier ne possède pas de parties alors le script exporte une partie par instrument.

## Structure des dérivations [Partiellement implémenté]

L'arborescence de la dérivation est la suivante:
```
<hash>-musescore-<nom du morceau>
│   <nom du morceau>.mscz
│   <nom du morceau>_full_score.pdf
│
└───pdf_parts
│   │   <nom du morceau>_<instru>.pdf
│   │   ...
│   
│   # ci-dessous non implémenté
│
│   metadata.json
└───mscz_parts
    │   <nom du morceau>_<instru>.mscz
    │   ...
```

seul l'export en pdf est opérationnel
